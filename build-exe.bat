@echo off
pip install -r requirements.txt
pip install pyinstaller
pyinstaller --onefile --windowed --distpath "./dist/vox2ksh_gui" .\src\vox2ksh_gui.py
robocopy assets dist\vox2ksh_gui\assets /E

echo -----------------------------------------
echo If successful, find the application in dist\vox2ksh_gui.
pause