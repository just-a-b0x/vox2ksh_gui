# vox2ksh_gui
A WIP `.vox` to `.ksh` converter. Built off [my fork of TsFreddie's `vox2ksh` project](https://gitlab.com/just-a-b0x/VOX2KSH).
Tested with December 2020 archives.

![preview screenshot](assets/preview.png)

TsFreddie's converter isn't perfect, so he's made a repo with corrected chart files [here](https://github.com/TsFreddie/KSHConvertFix).

## Prerequisites
`ffmpeg` install either on PATH or set up in `Edit > Preferences`.

### Required PIP Modules
* `tk`
* `Pillow`
* `ffmpeg-python`

## Cloning
`git clone --recurse-submodules -j8 https://gitlab.com/just-a-b0x/vox2ksh_gui`

## Usage
```
cd src
python3 vox2ksh_gui.py
```

or

```
cd src
chmod +x vox2ksh_gui.py
./vox2ksh_gui.py # (already shebang'd)
```

## Building Locally
Make sure you have Python and PIP installed.

Windows: run `build-exe.bat`  
Linux/macOS/other UNIX: run `build-unix.sh` (may need to `chmod +x` it first)

You will find the resultant executable in the `dist` directory.

## TODO
* GH Actions pyinstaller
* FX sound samples (ie. drums, claps, etc.)
* special case where each chart of a song has its own audio file
* searching/filtering
* proper backgrounds
* proper preview audio
