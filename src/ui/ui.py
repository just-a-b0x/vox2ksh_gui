import traceback

from tkinter import *
from tkinter import ttk, filedialog, messagebox

import gbl
import config

from util import *
from database import Database
from convert import *

# windows and panels
from ui.songpreview import SongPreview
from ui.songlist import SongList
from ui.convertwindow import ConvertWindow
from ui.preferenceswindow import PreferencesWindow

# Directory Setting Window
# Once done selecting, access selected path with .path.get()
class InitDirectorySelect(ttk.Frame):
    def __init__(self, master=None):
        super().__init__(master, padding='10 10')
        self.master = master
        self.pack()
        self.ok_clicked = False
        self.list = None
        self.create_widgets()
    
    def create_widgets(self):
        self.master.title("Set your \"contents\" directory")

        self.str_path = StringVar(None, config.content_path)
        self.str_path.trace('w', self.path_changed)
        self.entry_path = ttk.Entry(self, width=60, textvariable=self.str_path)
        self.entry_path.pack()

        self.btn_continue = ttk.Button(self, text="Continue", command=self.continue_pressed)
        self.btn_continue.pack(side=RIGHT)

        self.btn_file_dialog = ttk.Button(self, text="Browse", command=self.dir_dialog)
        self.btn_file_dialog.pack(side=RIGHT)

        self.path_changed()

    def continue_pressed(self):
        try:
            open_contents_db(self.str_path.get())
        except FileNotFoundError:
            messagebox.showerror("Error", "{} could not be found.\nCheck that the entered path is correct (should end in \"contents\").".format(MUSIC_DB_PATH))
            return
        except Exception as err:
            print('Unknown error occured: {}'.format(type(err)))
            messagebox.showerror("Error", "Unknown error occured while opening {}.\n{}".format(MUSIC_DB_PATH, err))
            return
        self.ok_clicked = True
        config.content_path = self.entry_path.get()
        config.save()
        self.master.destroy()

    def dir_dialog(self):
        newPath = filedialog.askdirectory(initialdir = self.entry_path.get())
        if newPath != "":
            self.str_path.set(newPath)

    def path_changed(self, *args):
        if len(self.str_path.get()) > 0:
            self.btn_continue.state(['!disabled'])
        else:
            self.btn_continue.state(['disabled'])

# main application window
class MainApp(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.master = master
        self.master.title("vox2ksh_gui")
        self.pack(expand=YES, fill=BOTH)
        self.create_widgets()
        self.focus_force()

    def create_widgets(self):
        self.menu = Menu(self)
        self.menuFile = Menu(self.menu, tearoff=0)
        self.menuEdit = Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label = 'File', menu = self.menuFile)
        self.menu.add_cascade(label = 'Edit', menu = self.menuEdit)
        # self.menuFile.add_command(label='Open contents...')
        self.menuEdit.add_command(label='Preferences', command=self.open_preferences)
        self.songList = SongList(self)
        self.songPreview = SongPreview(self)
        self.pnlOptions = ttk.Frame(self, padding='10 10')
        self.pnlOptions.btnSelAll = ttk.Button(self.pnlOptions, text='Select All', command=self.songList.select_all)
        self.pnlOptions.btnConvert = ttk.Button(self.pnlOptions, text='Convert', command=self.on_convert_pressed, state=DISABLED)
        self.pnlOptions.lblSelCnt = ttk.Label(self.pnlOptions)
        self.refresh_selection_counter()

        self.master.winfo_toplevel().config(menu=self.menu)
        self.songList.grid(row=0, column=0, sticky='nsew')
        self.songPreview.grid(row=1, column=0, sticky='w', padx=10, pady=10)
        self.pnlOptions.grid(row=2, sticky='we')
        self.pnlOptions.btnConvert.pack(side=RIGHT)
        self.pnlOptions.btnSelAll.pack(side=RIGHT)
        self.pnlOptions.lblSelCnt.pack(side=LEFT)

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        self.songList.bind('<<selectedSong>>', self.on_song_selected)
    
    def refresh_selection_counter(self):
        size = '{}/{}'.format(len(gbl.songIdSelections), len(gbl.songDb.songs))
        if len(gbl.songIdSelections) == 1:
            self.pnlOptions.lblSelCnt.configure(text='Selected {} song'.format(size))
        else:
            self.pnlOptions.lblSelCnt.configure(text='Selected {} songs'.format(size))

    def on_song_selected(self, ev):
        self.songPreview.set_song()
        if len(gbl.songIdSelections) > 0:
            self.pnlOptions.btnConvert.state(['!disabled'])
        else:
            self.pnlOptions.btnConvert.state(['disabled'])
        size = '{}/{}'.format(len(gbl.songIdSelections), len(gbl.songDb.songs))
        self.refresh_selection_counter()

    def on_convert_pressed(self):
        try:
            test_ffmpeg()
        except FileNotFoundError:
            messagebox.showerror('Error', 'Could not find FFmpeg for conversion. Check that it\'s in your PATH or set its location in Edit > Preferences.')
            return
        except Exception as e:
            messagebox.showerror('Error', 'Unknown error occured initializing FFmpeg. Bad executable?')
            print(e.with_traceback(None))
            return

        convTl = Toplevel(self)
        conversionWindow = ConvertWindow(convTl)
        convTl.mainloop()

    def open_preferences(self):
        prefTl = Toplevel(self)
        prefWin = PreferencesWindow(prefTl)
        prefTl.mainloop()

def ui_start():
    root = Tk()
    root.resizable(False, False)
    # set directory
    dirSel = InitDirectorySelect(master=root)
    dirSel.mainloop()
    if not dirSel.ok_clicked: return

    del root
    root = Tk()

    contentPath = dirSel.str_path.get()
    if not content_path_valid(contentPath):
        print("Bad path!")
        return

    try:
        gbl.songDb = Database(contentPath)
    except Exception:
        messagebox.showerror('Error while initializing the database', traceback.format_exc())
        return
    
    root.winfo_toplevel().withdraw()
    messagebox.showinfo('Database initialization completed', 'This application is still in development.\nThings may be broken!')
    root.winfo_toplevel().deiconify()

    # start main application
    main = MainApp(root)
    main.mainloop()
    