## The preferences window, opened from MainApp.

from tkinter import *
from tkinter import ttk, filedialog

import config

class PreferencesWindow(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.master = master
        self.master.title('Preferences')
        self.master.resizable(False, False)
        self.pack(expand=YES, fill=BOTH, padx=10, pady=10)
        self.master.protocol('WM_DELETE_WINDOW', self.on_close)
        self.create_widgets()

        self.grab_set()
        self.focus_force()

    def create_widgets(self):
        self.strFFmpeg = StringVar(None, value=config.ffmpegPath)
        
        self.ffmpegLabel = ttk.Label(self, text='FFmpeg Executable')
        self.entFFmpeg = ttk.Entry(self, width=60, textvariable=self.strFFmpeg)
        self.lblFFmpegNote = ttk.Label(self, text='Leave blank to use FFmpeg from PATH')
        self.btnFFmpegBrowse = ttk.Button(self, text="Browse", command=self.ffmpeg_browse)

        self.ffmpegLabel.grid(row = 0, column = 0, sticky = E, padx = (0, 5))
        self.entFFmpeg.grid(row = 0, column = 1)
        self.lblFFmpegNote.grid(row = 1, column = 1, sticky = W)
        self.btnFFmpegBrowse.grid(row = 1, column = 1, sticky = E, pady = (5, 0))

    def ffmpeg_browse(self):
        newFile = filedialog.askopenfilename(initialfile=self.entFFmpeg.get())
        if newFile != "":
            self.strFFmpeg.set(newFile)

    def on_close(self):
        config.ffmpegPath = self.entFFmpeg.get()
        config.save()
        self.grab_release()
        self.master.destroy()