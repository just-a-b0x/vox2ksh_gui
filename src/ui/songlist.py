## The song list panel.

from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk

import gbl
from util import *
from structs import *

class SongList(ttk.Frame):
    def __init__(self, master, conversion=False):
        super().__init__(master)
        self.master = master
        self.conversion = conversion
        self.create_widgets()
        self.refreshList()

    def create_widgets(self):
        columns = ('Title', 'Artist', 'Source')
        self.tblSong = ttk.Treeview(self, columns=columns)
        for col in columns:
            self.tblSong.heading(col, text=col, command=lambda _col=col: \
                self.treeview_sort_column(_col, False))
        # self.tblSong.heading('#0', text='ID', command=lambda _col='#0': \
        #     self.treeview_sort_column(_col, False))
        
        self.tblSong.heading('#0', text='ID')
        # self.tblSong.heading('#1', text='Title')
        # self.tblSong.heading('#2', text='Artist')
        # self.tblSong.heading('#3', text='Source')
        self.tblSong.column('#0', width=80, stretch=NO)
        self.tblSong.column('#1', stretch=YES)
        self.tblSong.column('#2', stretch=YES)
        self.tblSong.column('#3', width=100,stretch=NO)
        self.tblSong.tag_configure('done', foreground='gray')
        self.tblSong.tag_configure('error', foreground='red')
        self.tblSong.bind('<<TreeviewSelect>>', self.onListSel)
        
        self.scrlbrSong = ttk.Scrollbar(self, command=self.tblSong.yview)
        self.tblSong.configure(yscrollcommand = self.scrlbrSong.set)
        self.tblSong.grid(column=0, row=0, sticky='nsew')
        self.scrlbrSong.grid(column=1, row=0, sticky='ns')

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        
    def refreshList(self):
        if self.conversion:
            self.list = gbl.songIdSelections
            self.tblSong.configure(selectmode='none')
        else:
            self.list = gbl.songDb.songs
        for elem in self.list:
            song = gbl.songDb[elem]
            self.tblSong.insert('', 'end', text=elem, values=(song.title, song.artist, song.version))
    
    def onListSel(self, ev):
        if self.conversion: return

        sels = self.tblSong.selection()
        gbl.songIdSelections = []
        for elem in sels:
            gbl.songIdSelections.append(self.tblSong.item(elem, 'text'))

        # for song preview
        selId = self.tblSong.item(self.tblSong.focus(), 'text')
        if selId != '':
            gbl.songSelected = gbl.songDb[selId]
        else:
            gbl.songSelected = None

        self.event_generate('<<selectedSong>>')
    
    def select_all(self):
        self.tblSong.selection_set(self.tblSong.get_children())

    # ref: https://stackoverflow.com/questions/1966929/tk-treeview-column-sort
    def treeview_sort_column(self, col, reverse):
        l = [(self.tblSong.set(k, col).lower(), k) for k in self.tblSong.get_children('')]
        l.sort(reverse=reverse)

        # rearrange items in sorted positions
        for index, (val, k) in enumerate(l):
            self.tblSong.move(k, '', index)

        # reverse sort next time
        self.tblSong.heading(col, command=lambda: \
            self.treeview_sort_column(col, not reverse))

    def set_diff(self, newDiff: Difficulty = None):
        self.lblDif.configure(text=newDiff.tag if newDiff != None else '---')
        self.lblLvl.configure(text=newDiff.num if newDiff != None else '---')
        self.varEff.set(newDiff.effector if newDiff != None else '---')

        if newDiff == None:
            img = Image.open("./assets/jacket-placeholder.png")
            imgR = img.resize((80,80))
            self.imgTk = ImageTk.PhotoImage(imgR)
        else: # set self.imgTk to be jacket
            img = Image.open("{}/{}/{}/{}".format(gbl.songDb.contentPath, MUSIC_PATH, gbl.songSelected.folderSDVX, newDiff.illustPath))
            imgR = img.resize((80,80))
            self.imgTk = ImageTk.PhotoImage(imgR)
        self.lblImg.configure(image=self.imgTk)
