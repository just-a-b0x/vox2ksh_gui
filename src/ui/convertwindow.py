## The conversion window, shown after user has confirmed their song selection(s).

from tkinter import *
from tkinter import ttk, filedialog, messagebox

from threading import Thread

import gbl
import config
from convert import *
from ui.songlist import SongList

class ConvertWindow(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.master = master
        self.master.title('Conversion')
        self.pack(expand=YES, fill=BOTH)
        self.create_widgets()
        self.master.master.master.withdraw() # hide main window
        self.master.winfo_toplevel().protocol("WM_DELETE_WINDOW", self.on_close)
        self.focus_force()

        self.convertInProgress = False
        self.interruptConvert = False

        # check for EG charts' presence; warn about lasers
        for id in gbl.songIdSelections:
            print(gbl.songDb[id].version)
            if gbl.songDb[id].version == "6":
                self.master.winfo_toplevel().withdraw()
                messagebox.showwarning('Conversion', 'Exceed Gear chart(s) selected. Lasers in Exceed Gear charts may convert incorrectly!')
                self.master.winfo_toplevel().deiconify()
    
    def create_widgets(self):
        self.strProgress = StringVar()
        self.strPath = StringVar(None, config.exportPath)
        self.strPath.trace('w', self.path_changed)

        self.lblMsg = ttk.Label(self, text='The following songs will be converted:', justify='left')
        self.tblList = SongList(self, conversion=True)
        self.progressBar = ttk.Progressbar(self, maximum=len(gbl.songIdSelections), mode='determinate', orient=HORIZONTAL)
        self.lblProgress = ttk.Label(self, textvariable=self.strProgress)
        self.pnlOptions = ttk.Frame(self, padding='10 10')
        self.lblPath = ttk.Label(self.pnlOptions, text='Export to', justify='left')
        self.entPath = ttk.Entry(self.pnlOptions, width=60, textvariable=self.strPath)
        self.btnBrowse = ttk.Button(self.pnlOptions, text="Browse", command=self.dir_dialog)
        self.btnConvert = ttk.Button(self.pnlOptions, text='Convert', command=self.begin_conversion)
        self.btnCancel = ttk.Button(self.pnlOptions, text='Back', command=self.on_close)

        self.lblMsg.pack()
        self.tblList.pack(expand=YES, fill=BOTH)
        self.progressBar.pack(fill=X, padx=10)
        self.lblProgress.pack()
        self.pnlOptions.pack(anchor=E, fill=X)
        self.lblPath.pack(anchor=W)
        self.entPath.pack(fill=X)
        self.btnBrowse.pack(side=LEFT)
        self.btnConvert.pack(side=RIGHT)
        self.btnCancel.pack(side=RIGHT)

        if len(self.strPath.get()) > 0:
            self.btnConvert.state(['!disabled'])
        else:
            self.btnConvert.state(['disabled'])

    def on_close(self):
        if not self.convertInProgress:
            self.master.destroy()

    def dir_dialog(self):
        newPath = filedialog.askdirectory(initialdir=self.entPath.get())
        if newPath != "":
            self.strPath.set(newPath)

    def path_changed(self, *pargs):
        if len(self.strPath.get()) > 0:
            self.btnConvert.state(['!disabled'])
        else:
            self.btnConvert.state(['disabled'])

    def begin_conversion(self):
        def stop_conversion():
            self.interruptConvert = True

        config.exportPath = self.entPath.get()
        config.save()
        # disable widgets
        self.entPath.state(['disabled'])
        self.btnBrowse.state(['disabled'])
        self.btnConvert.state(['disabled'])
        self.btnCancel.configure(text='Stop', command=stop_conversion)
        
        self.convertInProgress = True
        # convert files in a separate thread
        Thread(target=self.convert).start()

    def convert(self):
        finishStat = 'Done!'
        self.progressBar['value'] = 0
        tblList = self.tblList.tblSong.get_children()

        for idx, id in enumerate(gbl.songIdSelections):
            self.strProgress.set('Converting ID {} [{}/{}]'.format(id, idx+1, len(gbl.songIdSelections)))
            self.tblList.tblSong.selection_set(tblList[idx])
            self.update()
            status = -1
            try:
                status = 0
                create_song_directory(id)
                status = 1
                convert_chart(id)
                status = 2
                convert_audio(id)
                self.tblList.tblSong.item(tblList[idx], tags='done')
            except Exception as err:
                # raise err # for debugging
                a = messagebox.showerror('Error {}'.format(CONVERT_STAT[status]), err)
                print(a)
                self.tblList.tblSong.item(tblList[idx], tags='error')
            
            self.progressBar['value'] += 1.0
            self.update()
            
            if self.interruptConvert:
                finishStat = 'Interrupted by user.'
                self.interruptConvert = False
                break
        
        self.tblList.tblSong.selection_set([])
        self.strProgress.set('{} [{}/{}]'.format(finishStat, idx+1, len(gbl.songIdSelections)))
        self.update()
        self.convert_end()

    def convert_end(self):
        self.convertInProgress = False
        # restore widgets
        self.entPath.state(['!disabled'])
        self.btnBrowse.state(['!disabled'])
        self.btnConvert.state(['!disabled'])
        self.btnCancel.configure(text='Back', command=self.on_close)

    def destroy(self):
        self.master.master.winfo_toplevel().deiconify()
        self.master.master.update()
        self.master.master.focus_force()
        super().destroy()