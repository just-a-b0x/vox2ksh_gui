#!/bin/bash

pip install -r requirements.txt
pip install pyinstaller
pyinstaller --onefile --windowed --distpath "./dist/vox2ksh_gui" ./src/vox2ksh_gui.py

cp -r assets dist/vox2ksh_gui/

echo
echo "If successful, find the application in dist/vox2ksh_gui."
